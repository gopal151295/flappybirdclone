package com.gopal151295.flappyclone;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

public class FlappyClone extends ApplicationAdapter {
	SpriteBatch batch;
	Texture [] birds;
	Texture background;
	int flapcount = 0;
	float birdY = 0;
//	boolean isGameStarted = false;
    int gameState;
	float velocity = 0;
	float gravity = 2f;
    Texture bottomTube;
    Texture topTube;
    private float gap = 350;
    private float tubeVelocity ;
    int tubeCount = 4;
    float[] topTubeX;
    float topTubeY;

    float[] bottomTubeX;
    float bottomTubeY;

    float maxTubeOffset;
    Random random;
    float[] tubeOffset;
    float tubeDistance;
    Circle birdCircle;
    ShapeRenderer shapeRenderer;

    Rectangle[] topTubeRectangle;
    Rectangle[] bottomTubeRectangle;
    int scoringTube = 0;
    int score;
    BitmapFont font;
    Texture gameOver;




    @Override
	public void create () {
		batch = new SpriteBatch();
        random = new Random();
        topTubeX = new float[tubeCount];
        bottomTubeX = new float[tubeCount];
        tubeOffset = new float[tubeCount];
		background = new Texture("bg.png");
		birds = new Texture[2];
		birds[0] = new Texture("bird.png");
		birds[1] = new Texture("bird2.png");
		bottomTube = new Texture("bottomtube.png");
		topTube = new Texture("toptube.png");
        bottomTube = new Texture("bottomtube.png");
        tubeDistance = Gdx.graphics.getWidth()/1.3f;
        score = 0;
        font = new BitmapFont();
        font.setColor(Color.BLACK);
        font.getData().setScale(10f);
        gameState = 0;

        birdCircle = new Circle();
        shapeRenderer = new ShapeRenderer();

        gameOver = new Texture("gameover.png");

        topTubeRectangle = new Rectangle[tubeCount];
        //topTubesRenderer = new ShapeRenderer[tubeCount];

        bottomTubeRectangle = new Rectangle[tubeCount];
        //bottomTubeRenderer = new ShapeRenderer[tubeCount];


        tubeVelocity = 6;
        maxTubeOffset = Gdx.graphics.getHeight() - gap - 200;

        startGame();




        topTubeY = Gdx.graphics.getHeight()/2+ gap/2;

        bottomTubeY = Gdx.graphics.getHeight()/2-bottomTube.getHeight() - gap/2;
    }

    private void startGame() {
        birdY = Gdx.graphics.getHeight()/2 -birds[flapcount].getHeight()/2;

        for (int i = 0; i < tubeCount; i++) {
            topTubeX[i] =  Gdx.graphics.getWidth() + i * tubeDistance;
            bottomTubeX[i] = Gdx.graphics.getWidth() +  i * tubeDistance;
            tubeOffset[i] = random.nextFloat() * maxTubeOffset - maxTubeOffset/2;

            topTubeRectangle[i] = new Rectangle();
            bottomTubeRectangle[i] = new Rectangle();



        }
    }

    @Override
	public void render () {

		batch.begin();
		batch.draw(background, 0,0,Gdx.graphics.getWidth(), Gdx.graphics.getHeight());


        batch.draw(birds[flapcount],
                Gdx.graphics.getWidth()/2 - birds[flapcount].getWidth()/2 ,
                birdY);

		if (gameState == 1){

			if (Gdx.input.justTouched()){
				velocity = -25;
            }


            for (int i = 0; i < tubeCount; i++) {

                if (topTubeX[i] < -topTube.getWidth() || bottomTubeX[i]< -bottomTube.getWidth()){
                    topTubeX[i] += tubeCount * tubeDistance;
                    bottomTubeX[i] += tubeCount * tubeDistance;
                    tubeOffset[i] = random.nextFloat() * maxTubeOffset - maxTubeOffset/2;

                }else {

                    topTubeX[i] -= tubeVelocity;
                    bottomTubeX[i] -= tubeVelocity;
                }

                batch.draw(topTube,
                        topTubeX[i],
                        topTubeY + tubeOffset[i]);
                batch.draw(bottomTube,
                        bottomTubeX[i],
                        bottomTubeY +tubeOffset[i]);

            }
			if (birdY > 0 ){
				velocity = velocity + gravity;
				birdY = birdY - (velocity );

			}else{
                gameState = 2;
            }

		}else if (gameState == 0){
			if (Gdx.input.justTouched()){
                gameState =1;
			}
		}
        else if (gameState == 2){
            batch.draw(gameOver, Gdx.graphics.getWidth()/2 - gameOver.getWidth()/2, Gdx.graphics.getHeight()/2 - gameOver.getHeight()/2);
            if (Gdx.input.justTouched()){
                gameState = 0;
                startGame();
                scoringTube = 0;
                velocity = 0;
                score = 0;
            }
        }



		if (flapcount == 0){
			flapcount = 1;
		}else {
			flapcount = 0;
		}



        font.draw(batch,String.valueOf(score),100, 200);

        batch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.RED);

        for (int i = 0; i < tubeCount; i++) {
            topTubeRectangle[i].set(topTubeX[i],topTubeY + tubeOffset[i], topTube.getWidth(),topTube.getHeight() );
            bottomTubeRectangle[i].set(bottomTubeX[i],bottomTubeY + tubeOffset[i], bottomTube.getWidth(), bottomTube.getHeight());

            //shapeRenderer.rect(topTubeRectangle[i].x,topTubeRectangle[i].y,topTubeRectangle[i].getWidth(), topTubeRectangle[i].getHeight());
            //shapeRenderer.rect(bottomTubeRectangle[i].x,bottomTubeRectangle[i].y, bottomTubeRectangle[i].getWidth(), bottomTubeRectangle[i].getHeight());
            if (Intersector.overlaps(birdCircle, topTubeRectangle[i]) || Intersector.overlaps(birdCircle, bottomTubeRectangle[i])){
                Gdx.app.log("Collision", "yes");
                gameState = 2;
            }

            if (topTubeX[scoringTube] + topTube.getWidth() < birdCircle.x - birdCircle.radius){
                score++;
                if (scoringTube < (tubeCount-1)){
                    scoringTube++;
                }
                else{
                    scoringTube = 0;
                }
                Gdx.app.log("score", String.valueOf(score));
            }


        }



        birdCircle.set(Gdx.graphics.getWidth()/2, birdY + birds[flapcount].getHeight()/2, birds[flapcount].getWidth()/2);
//        shapeRenderer.circle(birdCircle.x, birdCircle.y, birdCircle.radius);
        shapeRenderer.end();



    }
	
}
